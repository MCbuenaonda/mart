Vue.component('print-component', {
    name: 'print-component',
    template: `
    <div class="ticket">
    <div class="card" style="width: 24rem;">
    <div class="row">
    <div class="col-sm">
      <img class="logo_ticket" src="/img/LOGO_MACETINES_ART-negro.png" width="110px" padding="10px"/>
    </div>
    <div class="col-8 mt-1">
      <h5 class="card-title text-left">!Gracias por su compra¡</h5>
      <p class="card-text">
        <span class="font-weight-bold">Fecha :</span>
        {{detail.fecha_venta}}
      </p>
      <p class="card-text">
        <span class="font-weight-bold">Numero de nota :</span>
        {{detail.id_nota}}
      </p>
      <p class="card-text">
        <span class="font-weight-bold">Cliente :</span>
        {{detail.cliente}}
      </p>
    </div>
  </div>
  <div class="card-body">
  </div>
  <ul class="list-group list-group-flush">
    <li class="list-group-item" v-for="(item, index) in products">
      {{ index + 1 }} - {{ item.tipo_producto }} - {{item.tamanio}} </br>
      <div class="text-right font-weight-bold">
        {{ item.cantidad}} X \$ {{item.costo_final}} = \$ {{item.cantidad * item.costo_final}}
      </div>
    </li>
  </ul>
  <div class="card-body">
  <div class="text-right font-weight-bold font-italic">
  Total \$ {{detail.total_venta}}
  <br>
  {{boleto}}
</div>
  <button type="button" class="btn btn-info no-print" @click="print()" ><i class="fas fa-download fa-sm mr-2"></i>Imprimir</button>
  </div>
</div>
  </div>
    `,
    props: ['detail_ticket','products_list','id_boleto'],
    data() {
        return {
          detail: {},
          products: [],
          boleto:{}
        }
    },
    mounted() {
      this.assignData();
      // console.log(this.values);
    },
    methods: {
      assignData() {
        this.detail = JSON.parse(this.detail_ticket);
        this.products = JSON.parse(this.products_list);
        this.boleto = JSON.parse(this.id_boleto);
        console.log('detail: ',this.detail)
        console.log('detail: ',this.products)
      },
      print(){
        window.print();
      }
    },
    computed: {
    
    }
})