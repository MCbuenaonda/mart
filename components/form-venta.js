Vue.component('form-venta', {
    name: 'form-venta',
    template: `
    <div>
      <form class="" :action="action" method="post">
        <div v-if="label === 'ventaProducto'">
          <input-hidden :id="'objId_' + idobject" :value="idobject" name="idObject"></input-hidden>
          <input-hidden :id="label" :value="label" name="hidden"></input-hidden>

          <!--id_cliente -->
          <input-select id="id_cliente" name="data[nombre]" :list="cliente" label="Clientes" :values="values"></input-select>
          <!--id_mensajero -->
          <input-select id="id_mensajero" name="data[id_mensajero]" :list="mensajeria" label="Mensajeros" :values="values"></input-select>
          <!-- listafor -->
          <h5>Lista de Productos</h5>

          <div class="row" v-for="item in productos" :key="item.id" >
          <div class="col">
            <input-select id="id_producto" v-model="item.producto_value" :name="item.name" :list="producto" :label="item.label"></input-select>
          </div>

          <div class="col">
            <h5>&nbsp;</h5>
            <div class="input-group mb-3">
              <input type="text" @blur="get_total(item.id,item.piezas)" :name="item.name_pieza" v-model="item.piezas" class="form-control" placeholder="Número de piezas" aria-label="Recipient's username" aria-describedby="basic-addon2">
            <div class="input-group-append">
            <span class="input-group-text" id="basic-addon2">Piezas</span>
          </div>
        </div>
      </div>

      <div class="col">
        <h5>&nbsp;</h5>
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text">$</span>
          </div>
          <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" :name="item.name_total" v-model="item.total" >
          <button class="btn" @click='eliminar_producto(item.id,$event)'><i class="fa fa-trash"></i></button>
        </div>
      </div>
    </div>

    <div class="row">
      <button class="btn" @click='agregar_producto($event)'><i class="fa fa-plus"></i></button>
    </div>

    <div class="col">
        <h5>Total de venta</h5>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text">$</span>
            </div>
          <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" name="data[total_pago]" v-model="calculate_total" >
            <div class="input-group-append">
              <span class="input-group-text">.00</span>
            </div>
        </div>
    </div>
    <!--id_pago -->
    <input-select id="id_pago" name="data[id_pago]" :list="metodo" label="Metodo Pago" :values="values"></input-select>
    
    <!--id_boleto-->
    <input-select id="id_boleto" name="id_boleto" :list="boleto" label="Numero boletos" :values="values"></input-select>


    <button type="submit" class="btn btn-secondary btn-block mt-5">{{section}} {{label}}</button>
    </div>
    </form>
    </div>
    `,
    props: ['action', 'label', 'cat', 'values', 'section', 'idobject'],
    data() {
        return {
            cliente: {},
            mensajeria: {},
            productos: [],
            metodo: {},
            producto: {},
            total_pago: 0,
            boleto: {}
        }
    },
    mounted() {
      this.assignData();
      console.log(this.values);
    },
    methods: {
      assignData() {
        let objList = this.parseList;
        this.cliente = objList.cliente;
        this.mensajeria = objList.mensajeria;
        this.metodo = objList.metodo;
        this.producto = objList.producto;
        this.boleto = objList.boleto;
      },
      eliminar_producto(msj, event) {
        index = this.get_index_productos(msj);
        this.productos.splice(index,1);
        event.preventDefault()
      },
      agregar_producto(event) {
        let num = this.productos.length +1 ;
        this.productos.push({
          id: 'id_producto_'+num,
          name: `producto[id_producto_${num}]`,
          name_pieza: `producto[value_pieza_${num}]`,
          name_total: `producto[value_total_${num}]`,
          producto_value: 10,
          label: 'Producto '+num,
          value: 0,
          total:null,
          piezas:null
        })
        event.preventDefault()
      },
      imprimir_nota(event) {

      },
      get_total(id, piezas) {
        let id_p = this.get_index_productos(id);
        let product_select = this.get_index_cat_producto([this.productos[id_p].producto_value])
        let total = parseFloat(product_select.costo_final,2) * piezas;
        this.productos[id_p].total = total;
      },
      get_index_productos( id) {
       return  this.productos.findIndex(x => x.id === id );
      },
      get_index_cat_producto(id){
        return this.producto.find(x => x.id_producto == id);
      }
    },
    computed: {
      parseList() {
        return JSON.parse(this.cat);
      },
      calculate_total: function() {
        if (!this.productos) {
            return 0;
        }
        return this.productos.reduce(function (total, value) {
            return (parseFloat(total) + Number(value.total)).toFixed(2);
        }, 0);
    }
    }
})
