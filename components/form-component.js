Vue.component('form-component', {
    name: 'form-component',
    template: `
    <div>
      <form class="" :action="action" method="post" enctype="multipart/form-data">
        <div v-if="label === 'Cliente'">
          <input-hidden :id="'objId_' + idobject" :value="idobject" name="idObject"></input-hidden>
          <input-hidden :id="label" :value="label" name="hidden"></input-hidden>

          <!--nombre -->
          <input-text id="nombre" label="Nombre" name="data[nombre]" :value="values.nombre" placeholder="Nombre Cliente"></input-text>
          <!--lugar -->
          <input-text id="lugar" label="Lugar" name="data[lugar]" :value="values.lugar" placeholder="Ej. CDMX"></input-text>
          <!--telefono -->
          <input-text id="telefono" label="Telefono" name="data[telefono]" :value="values.telefono" placeholder="Ej. 55 5555 5555"></input-text>
          <!--id_medio -->
          <input-select id="id_medio" name="data[id_medio]" :list="medios" label="Medio" :value="values.id_medio"></input-select>
        </div>

        <div v-if="label === 'Producto'">
          <input-hidden :id="'objId_' + idobject" :value="idobject" name="idObject"></input-hidden>
          <input-hidden :id="label" :value="label" name="hidden"></input-hidden>
          <!-- costo_final -->
          <!--id_costo -->
          <input-select id="id_costo" name="data[id_costo]" :list="costos" label="Costo" :value="values.id_costo"></input-select>
          <!-- descripcion_producto -->
          <input-text id="descripcion" name="data[descripcion]" label="Descripción" placeholder="Ej. echeveria"></input-text>
          <!-- costo_final -->
          <input-text id="descripcion" name="data[costo_final]" label="Costo final"  placeholder="Ej. 120.50"></input-text>
          <!--id_unidad -->
          <input-select id="id_unidad" name="data[id_unidad]" :list="unidades" label="Unidad" :value="values.id_unidad"></input-select>
          <!--id_tipo -->
          <input-select id="id_tipo" name="data[id_tipo]" :list="tipos" label="Tipo" :value="values.id_tipo"></input-select>
          <!--id_tamanio -->
          <input-select id="id_tamanio" name="data[id_tamanio]" :list="tamanios" label="Tamaño" :value="values.id_tamanio"></input-select>
          <!--imagen -->
          <input-image id="imagen" label="Imagen" name="data[imagen]" :baseurl="baseurl" :section="section"></input-image>

          <img alt="" class="sombraBox" :src="baseurl + 'uploads/images/productos/' + values.imagen" style="border-style:solid;border-width:2px;border-radius:8px;" v-if="values">
        </div>

        <div v-if="label === 'Especie' || label === 'Costo' || label === 'Unidad' || label === 'Tipo' || label === 'Tamanio'">
          <input-hidden :id="label" :value="label" name="hidden"></input-hidden>
          <div v-if="section !== 'Editar'">
            <input-text id="descripcion" label="Descripcion" name="data[descripcion]" placeholder="Ej. descripcion uno"></input-text>
          </div>

          <div>
            <input-hidden :id="'objId_' + idobject" :value="idobject" name="idObject"></input-hidden>
            <div v-for="(val, prop) in values">
              <input-text :id="prop" :label="prop" :name="'data[' + prop +']'" :value="val"></input-text>
            </div>
          </div>
        </div>
        <button type="submit" class="btn btn-secondary btn-block mt-5">{{section}} {{label}}</button>
      </form>
    </div>
    `,
    props: ['action', 'label', 'cat', 'values', 'section', 'idobject', 'baseurl'],
    data() {
        return {
            costos: {},
            unidades: {},
            tipos: {},
            tamanios: {},
            medios: {}
        }
    },
    mounted() {
      this.assignData();
      console.log(this.values);
    },
    methods: {
      assignData() {
        let objList = this.parseList;
        this.costos = objList.costos;
        this.unidades = objList.unidades;
        this.tipos = objList.tipos;
        this.tamanios = objList.tamanios;
        this.medios = objList.medios;
      }
    },
    computed: {
      parseList() {
        return JSON.parse(this.cat);
      }
    }
})
