Vue.component('input-select', {
    name: 'input-select',
    template: `
    <div class="form-group">
      <label :for="id" class="font-weight-bold">{{label}}</label>
      <select class="form-control" @change="$emit('input', $event.target.value )" :id="id" :name="name" v-if="!checkFlag">
        <option :value="item[id]" v-for="item in list" :selected="(item[id] === value )">{{item.descripcion || item.nombre}}</option>
      </select>
      <p class="text-danger" v-if="checkFlag">
        Aun no hay especies agregadas &nbsp;
        <a :href="label" class="btn btn-sm btn-outline-primary">
          <i class="fas fa-plus"></i> {{label}}
        </a>
      </p>
    </div>
    `,
    props: ['id', 'list', 'label', 'name', 'value'],
    data() {
        return {
            flagAdd: false,
            value: ''
        }
    },
    methods: {
      change_option (){
        console.log('change:',this.valueId);
      }
    },
    mounted() {
      console.log('vals:' + this.value);
    },
    computed: {
        checkFlag() {
            return (this.list.length > 0) ? false : true;
        }
    },
})
