Vue.component('input-hidden', {
    name: 'input-hidden',
    template: `
    <div class="form-group">
        <input type="hidden" class="form-control" :id="id" :name="name" :value="dataValue">
    </div>
    `,
    props: ['id', 'value', 'name'],
    data() {
        return {
          dataValue: this.value
        }
    },
    mounted() {}
})
