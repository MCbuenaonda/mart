Vue.component('input-text', {
    name: 'input-text',
    template: `
    <div class="form-group">
        <label :for="id" class="font-weight-bold">{{label}}</label>
        <input type="text" class="form-control" :id="id" :name="name" :placeholder="placeholder" :value="value" :readonly="flagInput">
    </div>
    `,
    props: ['id', 'label', 'placeholder', 'name', 'value', 'ro'],
    data() {
        return {}
    },
    mounted() {},
    computed: {
      flagInput() {
        return (this.id === 'descripcion' || this.id === 'nombre' || this.id === 'lugar' || this.id === 'telefono')? false : true ;
      }
    }
})
