Vue.component('input-image', {
    name: 'input-image',
    template: `
    <div>
      <div class="form-group">
        <label :for="id" class="font-weight-bold">{{label}}</label>
        <input :id="id" :name="name" type="file" class="file">
      </div>
    </div>
    `,
    props: ['id', 'label', 'name', 'value'],
    data() {
        return {}
    },
    mounted() {
      $("#" + this.id).fileinput({showCaption: false, showUpload: false, browseLabel: 'Examinar &hellip;'});
    },
    methods: {}
})
