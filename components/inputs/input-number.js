Vue.component('input-number', {
    name: 'input-number',
    template: `
    <div class="form-group">
        <label :for="id" class="font-weight-bold">{{label}}</label>
        <input type="number" class="form-control" :id="id" :name="name" :placeholder="placeholder">
    </div>
    `,
    props: ['id', 'label', 'placeholder', 'name'],
    data() {
        return {}
    },
    mounted() {}
})
