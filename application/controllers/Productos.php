<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller {
	public $plantilla = '_private';

	public function __construct(){
		parent::__construct();
		$this->load->helper('url');

		// Helpers
		// $this->load->helper('uploadfile');
		// $this->load->helper('xls');
		// Librerías
		// $this->load->library('fields');
		$this->load->library('alertas');
		$this->load->library('parserdata');
		// $this->load->library('labels');
		// Modelos
		$this->load->model('costom');
		$this->load->model('especiem');
		$this->load->model('unidadm');
		$this->load->model('tipom');
		$this->load->model('tamaniom');
		$this->load->model('productom');
		$this->load->model('clientem');
		$this->load->model('mediom');
		// Log de data
		$this->output->enable_profiler(false);
	}

	public function index() {
		redirect('productos/inicio');
	}

	function inicio(){
		$this->_valid_session();
		$label = $this->uri->segment(3);
		$table = strtolower($label).'m';
		$data['data']['label'] = $label;
		$data['data']['list'] = $this->$table->all();
		$this->load->view($this->plantilla, $data);
	}

	function agregar() {
		$this->_valid_session();
		$label = $this->uri->segment(3);
		$data['data']['catalogo'] = $this->_getCatalogo();
		$data['data']['label'] = $label;
		$this->load->view($this->plantilla, $data);
	}

	function editar() {
		$this->_valid_session();
		$label = $this->uri->segment(3);
		$idItem = $this->uri->segment(4);
		$table = strtolower($label).'m';
		$data['data']['idObject'] = $idItem;
		$data['data']['object'] = $this->$table->search(['id_'.strtolower($label) => $idItem]);
		$data['data']['catalogo'] = $this->_getCatalogo();
		$data['data']['label'] = $label;
		$this->load->view($this->plantilla, $data);
	}


	function doAgregar() {
		$data = $this->input->post('data');
		$hidden = $this->input->post('hidden');
		$table = strtolower($hidden).'m';
		if($hidden == "Producto"){
			$filename = $this->_saveImage($data['descripcion'], $_FILES['data']);
			$data['imagen'] = $filename;
		}
		$this->$table->insert($data);
		$this->alertas->notificar('info', 'Se guardaron los datos correctamente!.');
		redirect('productos/inicio/'.$hidden);
	}

	function doEditar() {
		$data = $this->input->post('data');
		$hidden = $this->input->post('hidden');
		$idObject = $this->input->post('idObject');
		$table = strtolower($hidden).'m';
		if (isset($_FILES['data'])) {
			$filename = $this->_saveImage($data['descripcion'], $_FILES['data']);
			$data['imagen'] = $filename;
		}
		$this->$table->update(['id_'.strtolower($hidden) => $idObject], $data);
		$this->alertas->notificar('info', 'Se actualizaron los datos correctamente!.');
		redirect('productos/inicio/'.$hidden);
	}

	/* Private functions */

	/* Funcion que retorna un catalogo de stock*/
	function _getCatalogo() {
		$objCat = new stdClass;
		$objCat->costos = $this->costom->all();
		$objCat->unidades = $this->unidadm->all();
		$objCat->tipos = $this->tipom->all();
		$objCat->tamanios = $this->tamaniom->all();
		// $objCat->medios = $this->mediom->all();
		return $objCat;
	}

	function _saveImage($name, $imagen_file) {
		$base_path 	= 'uploads/images/productos';
		if ( !file_exists ( $base_path ) ) {
			mkdir('./' . $base_path, 0777, true);
			chmod('./' . $base_path, 0777);
		}
		$file = $base_path . '/' . $imagen_file['name']['imagen'];
		move_uploaded_file($imagen_file['tmp_name']['imagen'], $file);
		return $imagen_file['name']['imagen'];
	}


	function _valid_session(){
		if ($this->session->userdata('admin')) {
			return true;
		}else{
			$this->alertas->notificar('error', 'Para ingresar inicie sesion.');
			redirect('admin/login');
		}
	}
}
