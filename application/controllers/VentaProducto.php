<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VentaProducto extends CI_Controller {
	public $plantilla = '_private';
	public $id_boleto = 0;

	public function __construct(){
		parent::__construct();
		$this->load->helper('url');

		// Helpers
		// $this->load->helper('uploadfile');
		// $this->load->helper('xls');
		// Librerías
		// $this->load->library('fields');
		$this->load->library('alertas');
		$this->load->library('parserdata');
		// $this->load->library('labels');
		// Modelos
		$this->load->model('ventaproductom');
		$this->load->model('ventam');
		$this->load->model('mensajeriam');
		$this->load->model('clientem');
		$this->load->model('productom');
		$this->load->model('metodom');
		$this->load->model('boletom');
		// Log de data
		$this->output->enable_profiler(false);
	}

	// este metodo manda a llamar a la vista
	public function index() {
		redirect('ventaProducto/inicio');
	}

	function inicio(){
		$this->_valid_session();
		$label = $this->uri->segment(1);
		$table = strtolower($label).'m';
		$data['data']['label'] = $label;
		$data['data']['list'] = $this->$table->all();
		$data['data']['catalogo'] = $this->_getCatalogo();
		$this->load->view($this->plantilla, $data);
	}

	function list(){
		$this->_valid_session();
		$label = $this->uri->segment(1);
		$table = strtolower($label).'m';
		$data['data']['label'] = $label;
		$data['data']['list'] = $this->$table->allVentas();
		$this->load->view($this->plantilla, $data);
	}


	/* Private functions */

	/* Funcion que retorna un catalogo de stock*/
	function _getCatalogo() {
		$objCat = new stdClass;
		$objCat->cliente = $this->clientem->all();
		$objCat->mensajeria = $this->mensajeriam->all();
		$objCat->producto = $this->productom->all();
		$objCat->metodo = $this->metodom->all();
		$objCat->boleto = $this->boletom->all();
		return $objCat;
	}

	function print_ticket(){
		$data = $this->input->post('data');
		$productos = $this->input->post('producto');
		$this->id_boleto = $this->input->post('id_boleto');
		$hidden = $this->input->post('hidden');
		$table = strtolower($hidden).'m';

		$venta = new stdClass;
		$venta->id_cliente = $data['nombre'];
		$venta->id_mensajero = $data['id_mensajero'];
		$venta->total = $data['total_pago'];
		$id_venta = $this->ventam->insert($venta);

		$ind = -1;
		$list_productos = [];
		foreach ($productos as $key => $value) {
			if (strpos($key, 'id_producto') > -1) {
				$ind++;
				$list_productos[$ind] = new stdClass;
				$list_productos[$ind]->id_producto = $value;
				$list_productos[$ind]->id_venta = $id_venta;
			}

			if (strpos($key, 'value_pieza') > -1) {
				$list_productos[$ind]->numero_producto = $value;
			}
		}

		$this->$table->insertBatch($list_productos);

		/* echo '<pre>'; print_r($venta);
		echo '<pre>'; print_r($list_productos);
		echo '<pre>'; print_r(count($list_productos));
		*/

		$success_insert = (count($list_productos) > 0) ? true:false;
		if ($success_insert){
			$this->_imprimir($id_venta);
		}



		// exit;
	}

	function reprint() {
		$id_venta = $this->uri->segment(3);
		$this->_imprimir($id_venta);
	}

	function _imprimir($id_venta){
		//echo '<pre> imprimir function';
		// echo '<pre>'; print_r($id_venta);
		//$detalle_venta = $this->ventam->print_venta($id_venta);
		//$detalle_producto = $this->ventaproductom->print_venta_producto($id_venta);
		// echo '<pre>'; print_r($detalle_venta);
		// echo '<pre>'; print_r($detalle_producto);

		$label = 'imprimir_ticket';
		$data['data']['label'] = $label;
		$data['data']['messages'] = 'Gracias por tu compra !';
		$data['data']['detail_ticket'] = $this->ventam->print_venta($id_venta);
		$data['data']['products_ticket'] = $this->ventaproductom->print_venta_producto($id_venta);
		$data['data']['id_boleto'] = ($this->id_boleto == 1) ? $this->id_boleto." boleto":$this->id_boleto." boletos";
		$this->load->view($this->plantilla, $data);

	}

	function _valid_session(){
		if ($this->session->userdata('admin')) {
			return true;
		}else{
			$this->alertas->notificar('error', 'Para ingresar inicie sesion.');
			redirect('admin/login');
		}
	}
}
