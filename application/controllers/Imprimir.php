<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Imprimir extends CI_Controller {
	public $plantilla = 'imprimir';

	public function __construct(){
		parent::__construct();
		$this->load->helper('url');

		// Helpers
		// $this->load->helper('uploadfile');
		// $this->load->helper('xls');
		// Librerías
		// $this->load->library('fields');
		$this->load->library('alertas');
		// $this->load->library('labels');
		// Modelos
		$this->load->model('adminm');
		// Log de data
		$this->output->enable_profiler(false);
	}

	public function index() {
		redirect('imprimir/inicio');
	}

	function inicio(){
		$data['data']['messages'] = 'saludos';
		$this->load->view($this->plantilla, $data);
	}



	function imprimir() {
		$data['data'] = false;
		$this->load->view($this->plantilla, $data);
	}
}
