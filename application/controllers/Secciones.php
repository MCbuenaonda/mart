<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Secciones extends CI_Controller {
	public $plantilla = '_private';

	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		// Librerías
		$this->load->library('alertas');
		$this->load->library('parserdata');
		// Modelos
		$this->load->model('clientem');
		$this->load->model('mediom');
		// Log de data
		$this->output->enable_profiler(false);
	}

	public function index() {
		redirect('secciones/inicio');
	}

	function inicio(){
		$this->_valid_session();
		$label = $this->uri->segment(3);
		$table = strtolower($label).'m';
		$data['data']['label'] = $label;
		$data['data']['list'] = $this->$table->all();
		$this->load->view($this->plantilla, $data);
	}

	function agregar() {
		$this->_valid_session();
		$label = $this->uri->segment(3);
		$data['data']['catalogo'] = $this->_getCatalogo();
		$data['data']['label'] = $label;
		$this->load->view($this->plantilla, $data);
	}

	function editar() {
		$this->_valid_session();
		$label = $this->uri->segment(3);
		$idItem = $this->uri->segment(4);
		$table = strtolower($label).'m';
		$data['data']['idObject'] = $idItem;
		$data['data']['object'] = $this->$table->search(['id_'.strtolower($label) => $idItem]);
		$data['data']['catalogo'] = $this->_getCatalogo();
		$data['data']['label'] = $label;
		$this->load->view($this->plantilla, $data);
	}

	function doAgregar() {
		$data = $this->input->post('data');
		$hidden = $this->input->post('hidden');
		$table = strtolower($hidden).'m';
		$this->$table->insert($data);
		$this->alertas->notificar('info', 'Se guardaron los datos correctamente!.');
		redirect('secciones/inicio/'.$hidden);
	}

	function doEditar() {
		$data = $this->input->post('data');
		$hidden = $this->input->post('hidden');
		$idObject = $this->input->post('idObject');
		$table = strtolower($hidden).'m';
		$this->$table->update(['id_'.strtolower($hidden) => $idObject], $data);
		$this->alertas->notificar('info', 'Se actualizaron los datos correctamente!.');
		redirect('secciones/inicio/'.$hidden);
	}


	/* Private functions */

	/* Funcion que retorna un catalogo de stock*/
	function _getCatalogo() {
		$objCat = new stdClass;
		$objCat->clientes = $this->clientem->all();
		$objCat->medios = $this->mediom->all();
		return $objCat;
	}

	function _valid_session(){
		if ($this->session->userdata('admin')) {
			return true;
		}else{
			$this->alertas->notificar('error', 'Para ingresar inicie sesion.');
			redirect('admin/login');
		}
	}
}
