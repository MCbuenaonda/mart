<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class home extends CI_Controller {
	public $plantilla = '_inicial';

	public function __construct(){
		parent::__construct();
		$this->load->helper('url');

		// Helpers
		// $this->load->helper('uploadfile');
		// $this->load->helper('xls');
		// Librerías
		// $this->load->library('fields');
		// $this->load->library('alertas');
		// $this->load->library('labels');
		// Modelos
		// $this->load->model('adminm');
		// Log de data
		$this->output->enable_profiler(false);
	}

	public function index() {
		redirect('home/inicio');
	}


	function inicio(){
		$data['data'] = false;
		$this->load->view($this->plantilla, $data);
	}

	public function cactus() {
		$this->load->view('cactus');
	}
}
