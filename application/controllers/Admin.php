<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public $plantilla = '_private';

	public function __construct(){
		parent::__construct();
		$this->load->helper('url');

		// Helpers
		// $this->load->helper('uploadfile');
		// $this->load->helper('xls');
		// Librerías
		$this->load->library('parserdata');
		$this->load->library('alertas');
		// $this->load->library('labels');
		// Modelos
		$this->load->model('adminm');
		// Log de data
		$this->output->enable_profiler(false);
	}

	public function index() {
		redirect('admin/inicio');
	}

	function inicio(){
		$this->_valid_session();
		$data = $this->adminm->all();
		$data['data']['messages'] = 'saludos';
		$this->load->view($this->plantilla, $data);
	}

	function login(){
		$data['data'] = false;
		$this->load->view($this->plantilla, $data);
	}

	function doLogin() {
		$data = $this->input->post('data');
		$data['password'] = md5($data['password']);
		$user = $this->adminm->search($data);
		if ($user) {
			$this->session->set_userdata('admin', $user);
			redirect('admin/inicio');
		} else {
			$this->alertas->notificar('error', 'Email o contraseña incorrectos.');
			redirect('admin/login');
		}
	}

	function salir() {
		$this->session->unset_userdata('admin');
		$this->alertas->notificar('info', 'Se ha cerrado la sesión.');
		redirect('admin/login');
	}

	function imprimir() {
		$data['data'] = false;
		$this->load->view($this->plantilla, $data);
	}

	/* Private functions */
	function _valid_session(){
		if ($this->session->userdata('admin')) {
			return true;
		}else{
			$this->alertas->notificar('error', 'Para ingresar inicie sesion.');
			redirect('admin/login');
		}
	}
}
