<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parserdata {

  function __construct(){
    $CI =& get_instance();
  }

  function parseField($field){
    return ucwords(str_replace("_", " ", $field));
  }
}
