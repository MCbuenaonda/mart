<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alertas {

  function __construct(){
    $CI =& get_instance();
  }

  function notificar($type, $msg){
    $CI =& get_instance();
    $CI->session->set_flashdata('msg_type', $type);
    $CI->session->set_flashdata('msg', $msg);
  }
}
