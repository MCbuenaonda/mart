<!-- Page Heading -->

<!-- Begin Page Content -->
<div class="container-fluid">
  <?php $this->load->view('includes/notificacion')?>

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><?php echo EMPRESA ?> - Administrador</h1>
    <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
  </div>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary"><?php echo $label ?></h6>
    </div>
    <div class="card-body">
      <div class="table table-responsive table-striped table-sm">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead class="bg-primary text-light p-5">
            <tr>
              <?php foreach ($list[0] as $key => $head): ?>
                <th class="p-2"><?php echo $this->parserdata->parseField($key) ?></th>
              <?php endforeach; ?>
              <th>Reimprimir</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($list as $key => $object): ?>
              <tr>
                <?php $idItem =  ''; ?>
                <?php foreach ($object as $prop => $value): ?>
                  <?php $idItem = ($prop == 'id_'.strtolower($label))? $value : $idItem ; ?>
                  <td><?php echo $value ?></td>
                <?php endforeach; ?>
                <td class="text-center">
                  <a href="<?php echo site_url('ventaProducto/reprint/'.$object->id_venta) ?>" class="btn btn-sm btn-secondary">
                    <i class="fas fa-print"></i>
                  </a>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>
<!-- /.container-fluid -->
