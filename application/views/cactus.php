<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('css/styles_public.css')?>" rel="stylesheet">
    <!-- Custom fonts for this template-->
  <link href="<?php echo base_url('vendor/fontawesome-free/css/all.min.css') ?>" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300&display=swap" rel="stylesheet">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200&display=swap" rel="stylesheet">
<script
  src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('.toggle').click(function(){
        $('ul').toggleClass('active');
      })
    })
</script>

  </head>

  <body style='background: none !important'>
  
    <header class="noindex">
        <a href="#" class="logo"><img src="<?php echo base_url('img/letras.png'); ?>"class="width=200" height="30" padding="10px"/></a>
        <a class="toggle">Menu</a>
        <ul class="active">
            <li><a href="#">INICIO</a></li>
            <li><a href="#">SUCULENTAS</a></li>
            <li><a href="#">CACTUS</a></li>
            <li><a href="#">LITHOPS</a></li>
            <li><a href="#">ARREGLOS</a></li>
            <li><a href="#">CONTACTO</a></li>
        </ul> 
</header>

    <div>
        <h2 class="title">Cactus</h2>
        <section></section>
        <div class="container-circle ">
            <div class="container-circle-content">
                <h2>jjjjjjj</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis nobis velit eos soluta a dolorum autem commodi error minima aliquid! Nesciunt, sunt vitae quasi unde ipsam voluptates quae corporis esse.</p>
            </div>
            
           <!-- inicio carrito-->
           <div class="container">
    <div class="row">
        <div class="col-md-4">
            <figure class="card card-product-grid card-lg"> <a href="#" class="img-wrap" data-abc="true"><img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1571750967/Ecommerce/ef192a21ec96.jpg"></a>
                <figcaption class="info-wrap">
                    <div class="row">
                        <div class="col-md-8"> <a href="#" class="title" data-abc="true">ASUS Laptop - 5GB RAM</a> </div>
                        <div class="col-md-4">
                            <div class="rating text-right"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </div>
                        </div>
                    </div>
                </figcaption>
                <div class="bottom-wrap"> <a href="#" class="btn btn-primary float-right" data-abc="true"> Buy now </a>
                    <div class="price-wrap"> <span class="price h5">$999</span> <br> <small class="text-success">Free shipping</small> </div>
                </div>
            </figure>
        </div>
        <div class="col-md-4">
            <figure class="card card-product-grid card-lg"> <a href="#" class="img-wrap" data-abc="true"><img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1571751108/Ecommerce/laptop-dell-xps-15-computer-monitors-laptops.jpg"></a>
                <figcaption class="info-wrap">
                    <div class="row">
                        <div class="col-md-8"> <a href="#" class="title" data-abc="true">Dell Laptop - 5GB RAM</a> </div>
                        <div class="col-md-4">
                            <div class="rating text-right"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </div>
                        </div>
                    </div>
                </figcaption>
                <div class="bottom-wrap"> <a href="#" class="btn btn-primary float-right" data-abc="true"> Buy now </a>
                    <div class="price-wrap"> <span class="price h5">$999</span> <br> <small class="text-success">Free shipping</small> </div>
                </div>
            </figure>
        </div>
        <div class="col-md-4">
            <figure class="card card-product-grid card-lg"> <a href="#" class="img-wrap" data-abc="true"><img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1571750722/Ecommerce/acer-v-17-nitro-realsense.jpg"></a>
                <figcaption class="info-wrap">
                    <div class="row">
                        <div class="col-md-8"> <a href="#" class="title" data-abc="true">Acer Laptop - 5GB RAM</a> </div>
                        <div class="col-md-4">
                            <div class="rating text-right"> <i class="fa fa-star"></i> </div>
                        </div>
                    </div>
                </figcaption>
                <div class="bottom-wrap"> <a href="#" class="btn btn-primary float-right" data-abc="true"> Buy now </a>
                    <div class="price-wrap"> <span class="price h5">$999</span> <br> <small class="text-success">Free shipping</small> </div>
                </div>
            </figure>
        </div>
        <div class="col-md-4">
            <figure class="card card-product-grid card-lg"> <a href="#" class="img-wrap" data-abc="true"><img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1571750391/Ecommerce/hp-17-x061nr-white-background.jpg"></a>
                <figcaption class="info-wrap">
                    <div class="row">
                        <div class="col-md-8"> <a href="#" class="title" data-abc="true">HP Laptop - 5GB RAM</a> </div>
                        <div class="col-md-4">
                            <div class="rating text-right"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </div>
                        </div>
                    </div>
                </figcaption>
                <div class="bottom-wrap"> <a href="#" class="btn btn-primary float-right" data-abc="true"> Buy now </a>
                    <div class="price-wrap"> <span class="price h5">$999</span> <br> <small class="text-success">Free shipping</small> </div>
                </div>
            </figure>
        </div>
        <div class="col-md-4">
            <figure class="card card-product-grid card-lg"> <a href="#" class="img-wrap" data-abc="true"><img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1571750338/Ecommerce/Laptop-3.jpg"></a>
                <figcaption class="info-wrap">
                    <div class="row">
                        <div class="col-md-8"> <a href="#" class="title" data-abc="true">Dell Laptop - 5GB RAM</a> </div>
                        <div class="col-md-4">
                            <div class="rating text-right"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </div>
                        </div>
                    </div>
                </figcaption>
                <div class="bottom-wrap"> <a href="#" class="btn btn-primary float-right" data-abc="true"> Buy now </a>
                    <div class="price-wrap"> <span class="price h5">$399</span> <br> <small class="text-success">Free shipping</small> </div>
                </div>
            </figure>
        </div>
        <div class="col-md-4">
            <figure class="card card-product-grid card-lg"> <a href="#" class="img-wrap" data-abc="true"><img src="https://res.cloudinary.com/dxfq3iotg/image/upload/v1571750967/Ecommerce/ef192a21ec96.jpg"></a>
                <figcaption class="info-wrap">
                    <div class="row">
                        <div class="col-md-8"> <a href="#" class="title" data-abc="true">Dell Laptop - 5GB RAM</a> </div>
                        <div class="col-md-4">
                            <div class="rating text-right"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> </div>
                        </div>
                    </div>
                </figcaption>
                <div class="bottom-wrap"> <a href="#" class="btn btn-primary float-right" data-abc="true"> Buy now </a>
                    <div class="price-wrap"> <span class="price h5">$599</span> <br> <small class="text-success">Free shipping</small> </div>
                </div>
            </figure>
        </div>
    </div>
</div>

           <!-- fin carrito-->



           


            <!-- FOOTER -->
        <footer class="footer_public">
            <div class="row text-center">
            <div class="col-md-4">
                <i class="fas fa-map-marked-alt footer-letras" ></i>
                Dirección
                <hr class="solid">
                Norte 80-A #4216 Col. La Malinche Gustavo A. Madero
            </div>
            <div class="col-md-4">
            <i class="fas fa-phone-alt footer-letras"></i> 
            Teléfono
            <hr class="solid">
            5564480621
            </div>
            <div class="col-md-4">
            <i class="fas fa-share-alt footer-letras"></i>
            <p>Redes sociales</p>
                <hr class="solid">
                <p>
                <ul class="redes_sociales_list">
                    <li>
                    <a class="collapse-item" href="<?php echo site_url('productos/inicio/Producto') ?>">
                        <i class="fab fa-facebook fa-2x"></i>
                    </a>
                    </li>
                    <li>
                    <a class="collapse-item" href="<?php echo site_url('productos/inicio/Producto') ?>">
                        <i class="fab fa-instagram fa-2x"></i>
                    </a>
                    </li>
                </ul>
                </p>
            </div>
            </div>
        </footer>
    </div>    
</div>        
  
  <script type="text/javascript">
    var section = document.querySelector('section');
    window.addEventListener('scroll', function(){
        var value =window.scrollY;
        section.style.clipPath= "circle("+value+"px at center)"

    })
</script>
<?php $this->load->view('includes/scripts'); ?>
    </body>
</html>
   
