<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('css/styles_public.css')?>" rel="stylesheet">
    <!-- Custom fonts for this template-->
  <link href="<?php echo base_url('vendor/fontawesome-free/css/all.min.css') ?>" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300&display=swap" rel="stylesheet">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200&display=swap" rel="stylesheet">
<script
  src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('.toggle').click(function(){
        $('ul').toggleClass('active');
      })
    })
</script>
 <!-- Custom styles for this template--><style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('/css/carousel.css'); ?>" rel="stylesheet">
  </head>

  <body>
  
    <header>
        <a href="#" class="logo"><img src="<?php echo base_url('img/logo.png'); ?>" height="80" padding="10px"/></a>
        <a class="toggle">Menu</a>
        <ul>
            <li><a href="#">INICIO</a></li>
            <li><a href="#">SUCULENTAS</a></li>
            <li><a href="<?php echo site_url('/home/cactus') ?>">CACTUS</a></li>
            <li><a href="#">LITHOPS</a></li>
            <li><a href="#">ARREGLOS</a></li>
            <li><a href="#">CONTACTO</a></li>
        </ul> 
 
    </header>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="<?php echo base_url('img/carrusel_1.jpg'); ?>" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="<?php echo base_url('img/Gallery.jpg'); ?>" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="<?php echo base_url('img/grenovia_circle.jpg'); ?>" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<main role="main">
  <!-- Marketing messaging and featurettes
  ================================================== -->
  <!-- Wrap the rest of the page in another container to center all the content. -->
  <div class="container marketing">
    <!-- Three columns of text below the carousel -->
    <div class="row">
      <div class="col-lg-4">
      <img class="img-circle" whidth="140px" height="140px" src="<?php echo base_url('/img/grenovia_circle.jpg'); ?>" />
      <h2>Suculentas</h2>
        <p>Hermosas suculentas propagadas en invernadero</p> 
        <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
      </div><!-- /.col-lg-4 -->
      <div class="col-lg-4">
      <img class="img-circle" whidth="140px" height="140px" src="<?php echo base_url('/img/grenovia_circle.jpg'); ?>" />
      <h2>Cactus</h2>
        <p>Diferentes cactaceas disponibles en Macetines Art.</p>
        <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
      </div><!-- /.col-lg-4 -->
      <div class="col-lg-4">
      <img class="img-circle" whidth="140px" height="140px" src="<?php echo base_url('/img/grenovia_circle.jpg'); ?>" />
      <h2>Lithops</h2>
        <p>Conoce las piedras vivientes, pleiospilos y lapidarias.</p>
        <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
      </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->
  </div><!-- /.container -->
</main>

<!-- Parallax -->
<p>Plantas que enamoran.</p>
<div class="parallax"></div>
<!-- Parallax -->

<main role="main">
    <!-- START THE FEATURETTES -->
    <hr class="featurette-divider">
    <div class="row" style="margin-right:0px; !important">
      <div class="col-md-7">
        <h2 class="featurette-heading">Suculentas Miniaturas<span class="text-muted">Coleccionables</span></h2>
        <p class="lead">Las suculentas miniaturas cuentan con raiz y todas son porpagadas en invernaderos </p>
      </div>
      <div class="col-md-5">
      <img  height="70%" src="<?php echo base_url('img/miniaturas.jpg'); ?>" />
    </div>
    </div>
    <hr class="featurette-divider">
    <div class="row featurette">
      <div class="col-md-7 order-md-2"> 
        <h2 class="featurette-heading">Cactus que deben estar<span class="text-muted">en tu colección</span></h2>
        <p class="lead">Cactaceas hermoas y fáciles de cuidar.</p>
      </div>
      <div class="col-md-5 order-md-1">
      <img  height="300px" src="<?php echo base_url('img/cactus_inicial.jpeg'); ?>" />
      </div>
    </div>
    <hr class="featurette-divider">
    <div class="row featurette">
      <div class="col-md-7">
        <h2 class="featurette-heading">Suculentas<span class="text-muted">Crassulas & sedums</span></h2>
        <p class="lead">De diferentes especies</p>
      </div>
      <div class="col-md-5">
      <img  height="300px" src="<?php echo base_url('img/caja_suculenta.jpeg'); ?>" />
    </div>
    </div>
    <!-- /END THE FEATURETTES -->
</main>
   
  
  <!-- FOOTER -->
  <footer class="footer_public">
    <div class="row text-center">
      <div class="col-md-4">
        <i class="fas fa-map-marked-alt footer-letras" ></i>
        Dirección
        <hr class="solid">
        Norte 80-A #4216 Col. La Malinche Gustavo A. Madero
      </div>
      <div class="col-md-4">
      <i class="fas fa-phone-alt footer-letras"></i> 
      Teléfono
      <hr class="solid">
      5564480621
    </div>
      <div class="col-md-4">
      <i class="fas fa-share-alt footer-letras"></i>
      <p>Redes sociales</p>
        <hr class="solid">
        <p>
          <ul class="redes_sociales_list">
            <li>
              <a class="collapse-item" href="<?php echo site_url('productos/inicio/Producto') ?>">
                <i class="fab fa-facebook fa-2x"></i>
              </a>
            </li>
            <li>
              <a class="collapse-item" href="<?php echo site_url('productos/inicio/Producto') ?>">
                <i class="fab fa-instagram fa-2x"></i>
              </a>
            </li>
          </ul>
        </p>
      </div>
    </div>
  </footer>
  

<?php $this->load->view('includes/scripts'); ?>
    </body>
</html>


