<div class="container-fluid">
  <!-- Page Heading -->
  <div class="no-print d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Ticket de compra</h1>
  </div>
  <div class="card-body">
      <print-component
      detail_ticket='<?php echo json_encode($detail_ticket[0]) ?>'
      products_list='<?php echo json_encode($products_ticket) ?>'
      id_boleto='<?php echo json_encode($id_boleto) ?>'>
    </print-component>
    </div>
</div>