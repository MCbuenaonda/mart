<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/vuex@3.0.1/dist/vuex.js"></script>
<script src="<?php echo base_url('components/inputs/input-text.js') ?>"></script>
<script src="<?php echo base_url('components/inputs/input-number.js') ?>"></script>
<script src="<?php echo base_url('components/inputs/input-select.js') ?>"></script>
<script src="<?php echo base_url('components/inputs/input-hidden.js') ?>"></script>
<script src="<?php echo base_url('components/inputs/input-image.js') ?>"></script>
<script src="<?php echo base_url('components/form-component.js') ?>"></script>
<script src="<?php echo base_url('components/form-venta.js') ?>"></script>
<script src="<?php echo base_url('components/print-component.js') ?>"></script>

<script>
new Vue({
    el: '#app',
    data:{}
});
</script>
<!-- Bootstrap core JavaScript-->
<script src="<?php echo base_url('vendor/jquery/jquery.min.js') ?>"></script>
<script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
<!-- Core plugin JavaScript-->
<script src="<?php echo base_url('vendor/jquery-easing/jquery.easing.min.js') ?>"></script>
<!-- Custom scripts for all pages-->
<script src="<?php echo base_url('js/sb-admin-2.min.js') ?>"></script>
<!-- Page level plugins -->
<script src="<?php echo base_url('vendor/chart.js/Chart.min.js') ?>"></script>
<!-- Page level custom scripts -->
<script src="<?php echo base_url('js/demo/chart-area-demo.js') ?>"></script>
<script src="<?php echo base_url('js/demo/chart-pie-demo.js') ?>"></script>

<!-- Page level plugins -->
<script src="<?php echo base_url('vendor/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url('vendor/datatables/dataTables.bootstrap4.min.js') ?>"></script>

<!-- Page level custom scripts -->
<script src="<?php echo base_url('js/demo/datatables-demo.js') ?>"></script>
