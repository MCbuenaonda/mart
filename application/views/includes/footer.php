<footer class="no-print sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; Macetines Art 2020</span>
        </div>
    </div>
</footer>