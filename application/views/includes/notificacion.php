<?php
if(!isset($class)){ $class  = 'alert-success'; }
if(!isset($notif)){ $notif  = "Notificación"; }

if($this->session->flashdata('error')==1 || $this->session->flashdata('msg_type')=='error' || (isset($msg_type) && $msg_type=='error')){
    $class = 'alert-danger';
    $notif = 'Error';
}

if($this->session->flashdata('warning')==1 || $this->session->flashdata('msg_type')=='warning' || (isset($msg_type) && $msg_type=='warning')){
    $class = 'alert-warning';
    $notif = 'Alerta';
}

if($this->session->flashdata('information')==1 ||  $this->session->flashdata('msg_type')=='information' || (isset($msg_type) && $msg_type=='information')){
    $class = 'alert-info';
    $notif = 'Información';
}

if($this->session->flashdata('msg')<>''){?>
  <div class="row" id="noti">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <br>
      <div id="alert_flash" class="alert <?php echo $class?> alert-dismissable" hidden>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b><?php echo $notif; ?>:</b> <?php echo $this->session->flashdata('msg'); ?>
      </div>
    </div>
  </div>
<?php }

if(isset($msg) && $msg<>''){?>
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <br>
      <div class="alert <?php echo $class?> alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b><?php echo $notif; ?>:</b> <?php echo $msg; ?>
      </div>
    </div>
  </div>
<?php }
