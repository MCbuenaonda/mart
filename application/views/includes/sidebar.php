<ul class="no-print navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo site_url('home/inicio') ?>">
      <div class="">
        <img src="<?php echo base_url('img/logo.png') ?>" alt="" width="100%">
      </div>
        <div class="sidebar-brand-text mx-2"><?php echo EMPRESA ?></div>
    </a>
    <!-- Divider -->
    <hr class="sidebar-divider my-0">
    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="<?php echo site_url('admin/inicio') ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">
    <!-- Heading -->
    <div class="sidebar-heading">
        SECCIONES
    </div>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#menu-clientes" aria-expanded="true" aria-controls="menu-productos">
            <i class="fas fa-users"></i>
            <span>Clientes</span>
        </a>
        <div id="menu-clientes" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?php echo site_url('secciones/inicio/Cliente') ?>">
                  <i class="fas fa-list-ol"></i>
                  Listado
                </a>
                <a class="collapse-item" href="<?php echo site_url('secciones/agregar/Cliente') ?>">
                  <i class="fas fa-plus-circle"></i>
                  Agregar
                </a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#menu-venta" aria-expanded="true" aria-controls="menu-productos">
            <i class="fas fa-users"></i>
            <span>Venta Producto</span>
        </a>
        <div id="menu-venta" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="<?php echo site_url('ventaProducto/list') ?>">
                <i class="fas fa-list-ol"></i>
                Listado
              </a>

              <a class="collapse-item" href="<?php echo site_url('ventaProducto/inicio') ?>">
                <i class="fas fa-plus-circle"></i>
                Venta
              </a>
            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">
    <!-- Heading -->
    <div class="sidebar-heading">
        STOCK
    </div>
    <!-- Nav Item - Productos -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#menu-productos" aria-expanded="true" aria-controls="menu-productos">
            <i class="fas fa-fw fa-spa"></i>
            <span>Productos</span>
        </a>
        <div id="menu-productos" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?php echo site_url('productos/inicio/Producto') ?>">
                  <i class="fas fa-list-ol"></i>
                  Listado
                </a>
                <a class="collapse-item" href="<?php echo site_url('productos/agregar/Producto') ?>">
                  <i class="fas fa-plus-circle"></i>
                  Agregar
                </a>
            </div>
        </div>
    </li>

    <!-- Nav Item - Costos -->
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#new-costo" aria-expanded="true" aria-controls="new-costo">
          <i class="fas fa-fw fa-dollar-sign"></i>
          <span>Costos</span>
      </a>
      <div id="new-costo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="<?php echo site_url('productos/inicio/Costo') ?>">
                <i class="fas fa-list-ol"></i>
                Listado
              </a>
              <a class="collapse-item" href="<?php echo site_url('productos/agregar/Costo') ?>">
                <i class="fas fa-plus-circle"></i>
                Agregar
              </a>
          </div>
      </div>
    </li>
    <!-- Nav Item - Unidades -->
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#new-unidad" aria-expanded="true" aria-controls="new-unidad">
          <i class="fas fa-fw fa-dot-circle"></i>
          <span>Unidades</span>
      </a>
      <div id="new-unidad" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="<?php echo site_url('productos/inicio/Unidad') ?>">
                <i class="fas fa-list-ol"></i>
                Listado
              </a>
              <a class="collapse-item" href="<?php echo site_url('productos/agregar/Unidad') ?>">
                <i class="fas fa-plus-circle"></i>
                Agregar
              </a>
          </div>
      </div>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#new-tipo" aria-expanded="true" aria-controls="new-tipo">
          <i class="fas fa-fw fa-tag"></i>
          <span>Tipos</span>
      </a>
      <div id="new-tipo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="<?php echo site_url('productos/inicio/Tipo') ?>">
                <i class="fas fa-list-ol"></i>
                Listado
              </a>
              <a class="collapse-item" href="<?php echo site_url('productos/agregar/Tipo') ?>">
                <i class="fas fa-plus-circle"></i>
                Agregar
              </a>
          </div>
      </div>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#new-tamanio" aria-expanded="true" aria-controls="new-tamanio">
          <i class="fas fa-fw fa-text-height"></i>
          <span>Tamaños</span>
      </a>
      <div id="new-tamanio" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
              <a class="collapse-item" href="<?php echo site_url('productos/inicio/Tamanio') ?>">
                <i class="fas fa-list-ol"></i>
                Listado
              </a>
              <a class="collapse-item" href="<?php echo site_url('productos/agregar/Tamanio') ?>">
                <i class="fas fa-plus-circle"></i>
                Agregar
              </a>
          </div>
      </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>
