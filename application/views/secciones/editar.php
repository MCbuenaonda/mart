<div class="container-fluid">
  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><?php echo EMPRESA ?> - Administrador</h1>
  </div>
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Editar <?php echo $label ?></h6>
    </div>
    <div class="card-body">
      <form-component
      action="<?php echo site_url('secciones/doEditar') ?>"
      label="<?php echo $label ?>" cat='<?php echo json_encode($catalogo) ?>'
      :values='<?php echo json_encode($object) ?>'
      section="Editar"
      :idobject="<?php echo $idObject ?>"
      baseurl="<?php echo base_url() ?>"></form-component>
    </div>
  </div>
</div>
