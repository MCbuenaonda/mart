<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<?php $this->load->view('includes/head'); ?>


<body id="page-top" class="<?php echo ($this->router->fetch_method() === 'login')? 'bg-primary' : '' ?>">

  <div id="app">
    <!-- Page Wrapper -->
    <div id="wrapper">

      <!-- Sidebar -->
      <?php if ($this->router->fetch_method() !== 'login'): ?>
        <?php $this->load->view('includes/sidebar'); ?>
      <?php endif; ?>
      <!-- End of Sidebar -->

      <!-- Content Wrapper -->
      <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content" class="<?php echo ($this->router->fetch_method() === 'login')? 'bg-primary' : '' ?>">

          <!-- Topbar -->
          <?php if ($this->router->fetch_method() !== 'login'): ?>
            <?php $user = $this->session->userdata('admin'); ?>
            <?php $this->load->view('includes/toolbar', $user); ?>
          <?php endif; ?>
          <!-- End of Topbar -->


          <!-- Begin Page Content -->
          <div class="p-4">
            <?php $this->load->view($this->router->fetch_class() .'/'. $this->router->fetch_method(), $data);?>
          </div>
          <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <?php if ($this->router->fetch_method() !== 'login'): ?>
          <?php $this->load->view('includes/footer'); ?>
        <?php endif; ?>
        <!-- End of Footer -->

      </div>
      <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Lista para irse?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Seleccione "Cerrar sesión" si está lista para finalizar su sesión actual.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
            <a class="btn btn-primary" href="<?php echo site_url('admin/salir') ?>">Cerrar sesión</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/plugins/piexif.min.js" type="application/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/plugins/sortable.min.js" type="application/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/fileinput.min.js"></script>
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.9/js/locales/LANG.js"></script> -->


  <script src="https://code.jquery.com/jquery-1.12.3.js"></script>
  <script>
  function showAlert(hideAfter) {
    $('#alert_flash').removeAttr('hidden');
    if(hideAfter) {

      setTimeout(fadeItOut, 5000);
    }
  }

  function fadeItOut() {
    $('#alert_flash').attr('hidden', true);
  }

  // Jquery has loaded
  $( document ).ready(function() {
    setTimeout(showAlert(1), 3000);
  });
  </script>

  <?php $this->load->view('includes/scripts'); ?>

</body>

</html>
