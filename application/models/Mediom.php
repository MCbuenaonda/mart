<?php
class mediom extends CI_Model {
  private $table = 'medio_venta';

  function __construct(){
    parent::__construct();
  }

  function all($where=false){
    if ($where) {
      return $this->db->where($where)->get($this->table)->result();
    }else{
      return $this->db->get($this->table)->result();
    }
  }

  function insert($data){
    $this->db->insert($this->table, $data);
  }

  function insertBatch($data) {
    $this->db->insert_batch($this->table, $data);
  }

  function update($where, $set){
    $this->db->where($where)->update($this->table, $set);
  }

  function search($where=false, $order=false){
    if ($order) {
      return $this->_order($where, $order);
    }else{
      return $this->db->where($where)->get($this->table)->row();
    }
  }

  function _order($where, $order){
    if ($where) {
      return $this->db->where($where)->order_by($order)->get($this->table)->row();
    }else{
      return $this->db->order_by($order)->get($this->table)->row();
    }
  }


}
