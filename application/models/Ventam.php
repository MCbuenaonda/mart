<?php
class ventam extends CI_Model {
  private $table = 'venta';

  function __construct(){
    parent::__construct();
  }

  function insert($data){
    $this->db->insert($this->table, $data);
    return $this->db->insert_id();
  }

  function update($where, $set){
    $this->db->where($where)->update($this->table, $set);
  }

  function all($where=false){
    if ($where) {
      return $this->db->where($where)->get($this->table)->result();
    }else{
      return $this->db->get($this->table)->result();
    }
  }

  function print_venta($id_venta){
    $this->db->select('venta.id_venta as id_nota, venta.fecha_alta as fecha_venta, descuento, total as total_venta');
    $this->db->select('c.nombre cliente');
    $this->db->select('me.nombre mensajero');
    $this->db->select('mv.descripcion medio_venta');
    $this->db->join('cliente c', 'c.id_cliente = venta.id_cliente', 'inner');
    $this->db->join('mensajeria me','me.id_mensajero = venta.id_mensajero', 'inner');
    $this->db->join('medio_venta mv','mv.id_medio = venta.id_medio', 'left');
    /* $this->db->join('producto p','');
    $this->db->join('unidad u','');
    $this->db->join('tamanio t',''); */
    $this->db->where('id_venta', $id_venta);
    return $this->db->get($this->table)->result();
  }

  function search($where=false, $order=false){
    if ($order) {
      return $this->_order($where, $order);
    }else{
      return $this->db->where($where)->get($this->table)->row();
    }
  }

  function _order($where, $order){
    if ($where) {
      return $this->db->where($where)->order_by($order)->get($this->table)->row();
    }else{
      return $this->db->order_by($order)->get($this->table)->row();
    }
  }


}
