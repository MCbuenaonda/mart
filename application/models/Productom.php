<?php
class productom extends CI_Model {
  private $table = 'producto';

  function __construct(){
    parent::__construct();
  }

  function insert($data){
    $this->db->insert($this->table, $data);
  }

  function update($where, $set){
    $this->db->where($where)->update($this->table, $set);
  }

  function all($where=false){
    $this->db->select('p.*, c.descripcion as costo, u.descripcion as unidad, t.descripcion as tipo, a.descripcion as tamanio')
    ->from('producto p')
    ->join('costo c', 'p.id_costo = c.id_costo')
    ->join('unidad u', 'p.id_unidad = u.id_unidad')
    ->join('tipo t', 'p.id_tipo = t.id_tipo')
    ->join('tamanio a', 'p.id_tamanio = a.id_tamanio');
    return $this->db->get()->result();
  }

  function search($where=false, $order=false){
    if ($order) {
      return $this->_order($where, $order);
    }else{
      return $this->db->where($where)->get($this->table)->row();
    }
  }

  function _order($where, $order){
    if ($where) {
      return $this->db->where($where)->order_by($order)->get($this->table)->row();
    }else{
      return $this->db->order_by($order)->get($this->table)->row();
    }
  }


}
