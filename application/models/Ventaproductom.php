<?php
class ventaproductom extends CI_Model {
  private $table = 'venta_producto';

  function __construct(){
    parent::__construct();
  }

  function insert($data){
    $this->db->insert($this->table, $data);
  }

  function insertBatch($data) {
    $this->db->insert_batch($this->table, $data);
  }

  function update($where, $set){
    $this->db->where($where)->update($this->table, $set);
  }

  function all($where=false){
    if ($where) {
      return $this->db->where($where)->get($this->table)->result();
    }else{
      return $this->db->get($this->table)->result();
    }
  }

  function allVentas(){
    $this->db->select('v.id_venta, v.descripcion, c.nombre as cliente, v.descuento, mv.descripcion as medio, v.total, v.ganancia, v.fecha_alta')
    ->from('venta v')
    ->join('cliente c','c.id_cliente = v.id_cliente')
    ->join('medio_venta mv','v.id_medio = mv.id_medio');
    return $this->db->get()->result();
  }

  function print_venta_producto($id_venta){
    $this->db->select('numero_producto cantidad, p.descripcion tipo_producto, u.descripcion unidad, t.descripcion tamanio, p.costo_final');
    $this->db->join('producto p','p.id_producto = venta_producto.id_producto');
    $this->db->join('unidad u','u.id_unidad = p.id_unidad','inner');
    $this->db->join('tamanio t','t.id_tamanio = p.id_tamanio','inner');
    $this->db->where('id_venta', $id_venta);
    return $this->db->get($this->table)->result();
  }

  function search($where=false, $order=false){
    if ($order) {
      return $this->_order($where, $order);
    }else{
      return $this->db->where($where)->get($this->table)->row();
    }
  }

  function _order($where, $order){
    if ($where) {
      return $this->db->where($where)->order_by($order)->get($this->table)->row();
    }else{
      return $this->db->order_by($order)->get($this->table)->row();
    }
  }


}
