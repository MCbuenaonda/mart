<?php
class mensajeriam extends CI_Model {
  private $table = 'mensajeria';
  
  function __construct(){
    parent::__construct();
  }

  function all($where=false){
    if ($where) {
      return $this->db->where($where)->get($this->table)->result();
    }else{
      return $this->db->get($this->table)->result();
    }
  }
  

}